# frozen_string_literal: true

json.extract! blogpost, :id, :author, :title, :content, :created_at, :updated_at
json.url blogpost_url(blogpost, format: :json)
