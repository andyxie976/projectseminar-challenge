# frozen_string_literal: true

class Blogpost < ApplicationRecord
  validates :author, presence: true
  validates :title, presence: true
  validates :content, presence: true
end
