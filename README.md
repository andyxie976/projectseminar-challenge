# README

The Seminar challenge is written in ruby on rails with a postgresql database, which supports the MVC design.
Since ruby on rails is a very save framework still some vulnerabilities could be implemented. In my case I implemented a blogforum on which the user can Create Read Update Delete (CRUD) posts. Furthermore, I allowed markup syntax for a prettier display which leads to possible vulnerabilities. Of course, there are possibilties to prevent these vulnerabilities.
