# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Blogpost.create([
                  { author: 'Loredana Ipsum', title: 'Bavaria ipsum', content: "Bavaria ipsum dolor sit amet Deandlgwand no g’hupft wia gsprunga i. Hob in da greana Au Goaßmaß ozapfa zua. Hob i an Suri de Namidog Spotzerl Kneedl sauba ozapfa is wuid dringma aweng. Barfuaßat in da is ma Wuascht, von ded nomoi des naa midanand Gamsbart af! Do Meidromml sammawiedaguad i naa, noch da Giasinga Heiwog samma Stubn sei zwoa. Prosd damischa Watschnbaam, Ewig und drei Dog auszutzeln Biazelt i hob di narrisch gean ned gscheid. Nackata baddscher Weißwiaschd Haberertanz, gelbe Rüam barfuaßat nia need Kuaschwanz nia need hogg di hera! A bissal wos gehd ollaweil i sog ja nix, i red ja bloß Brotzeit gschmeidig da, hog di hi wea ko, dea ko Stubn jedza? Nimmds gwiss jo mei is des schee und vasteh, schoo: Umma a so a Schmarn a Maß und no a Maß de Kuaschwanz aba.
Wia da Buachbinda Wanninger schüds nei Maibam, wui i hob di liab mim Radl foahn i hob di narrisch gean owe? Wo hi wea nia ausgähd, kummt nia hoam Reiwadatschi oans resch und, Mamalad Jodler woaß. Brodzeid guad Gams Schorsch mechad vasteh Prosd des is a gmahde Wiesn hogg ma uns zamm i bin a woschechta Bayer? I moan oiwei do legst di nieda a Prosit der Gmiadlichkeit, a geh. Heid gwiss measi a Mamalad Schaung kost nix, Gidarn kimmt Guglhupf do. Heimatland de Sonn singd Oachkatzlschwoaf wos Maderln i moan oiwei woaß blärrd. Hoggd Breihaus i mog di fei Schorsch weida schoo schoo Engelgwand schnacksln. Is fias Xaver ned woar nia need aasgem Broadwurschtbudn Woibbadinga Ramasuri Spotzerl. Pfundig nia Lewakaas Schmankal, Sauakraud oans, zwoa, gsuffa fias amoi gelbe Rüam Resi! Fünferl zwoa weida auffi gscheit, ?
Scheans ognudelt hogg ma uns zamm Musi Gschicht Greichats Sauakraud Edlweiss, wea ko, dea ko. Zidern is ma Wuascht wos Weiznglasl barfuaßat und glei wirds no fui lustiga Sauwedda Habedehre zua, Jodler koa. Jo mei is des schee i umananda Woibbadinga obacht! Imma muass hod bittschön sog i zua Leonhardifahrt nackata: Und Greichats eana Griasnoggalsubbm. Zua trihöleridi dijidiholleri a bravs, hob i an Suri imma amoi kumm geh samma i hab an i daad hoggd. Bittschön auf der Oim, da gibt’s koa Sünd Freibia zua eana, vui huift vui glei blärrd. Schdeckalfisch do legst di nieda Sauakraud gfreit mi, auf gehds beim Schichtl pfundig. Moand measi blärrd aasgem Gams samma amoi. Griasd eich midnand i hob di narrisch gean griasd eich midnand, aasgem. Trachtnhuat Prosd barfuaßat auf’d Schellnsau back mas Trachtnhuat wea ko, dea ko." },
                  { author: 'Steve Jobs', title: 'Who is Linus Torvalds?', content: "Linus Benedict Torvalds (/ˈliːnəs ˈtɔːrvɔːldz/ LEE-nəs TOR-vawldz,[3] Finland Swedish: [ˈliːnʉs ˈtuːrvɑlds] (About this soundlisten); born 28 December 1969) is a Finnish-American software engineer who is the creator and, historically, the main developer of the Linux kernel, used by Linux distributions and other operating systems such as Android. He also created the distributed version control system Git and the scuba dive logging and planning software Subsurface.
He was honored, along with Shinya Yamanaka, with the 2012 Millennium Technology Prize by the Technology Academy Finland in recognition of his creation of a new open source operating system for computers leading to the widely used Linux kernel[4]. He is also the recipient of the 2014 IEEE Computer Society Computer Pioneer Award[5] and the 2018 IEEE Masaru Ibuka Consumer Electronics Award.[6]" },
                  { author: 'Jeff Bezos', title: 'Who is Jack Ma?', content: "Jack Ma Yun[a] (Chinese: 马云; pinyin: mǎ yún; born 10 September 1964) is a Chinese business magnate, investor and philanthropist. He is the co-founder and former executive chairman of Alibaba Group, a multinational technology conglomerate. In addition, he co-founded Yunfeng Capital, a private equity firm. Ma is a strong proponent of an open and market-driven economy.
In 2017, Ma was ranked second in the annual World's 50 Greatest Leaders list by Fortune.[3] He has widely been considered as an informal global ambassador for Chinese business, and is an influential figure for the community of startup businesses.[4] In September 2018, he announced that he would retire from Alibaba and pursue educational work, philanthropy, and environmental causes;[5][6][7][8] the following year, Daniel Zhang succeeded him as executive chairman.[9]
As of April 2021, with a net worth of $51.5 billion, Ma is the third-wealthiest person in China (after Zhong Shanshan and Ma Huateng), as well as one of the wealthiest people in the world, ranked 26th by Bloomberg Billionaires Index.[2] In 2019, Forbes named Ma in its list of Asia's 2019 Heroes of Philanthropy for his work supporting underprivileged communities in China, Africa, Australia, and the Middle East.[5][10] " },
                  { author: 'Loredana Picard', title: 'Sister of Bavaria Ipsum',
                    content: "![Stormtroopocat](https://octodex.github.com/images/stormtroopocat.jpg 'The Stormtroopocat')" }
                ])
Blogpost.create({author: "Andy", title: "Syntax Highlighting in Markdown", content: "``` html
    <!DOCTYPE html>
    <html>
    <body>

    <h1>My First Heading</h1>
    
    <p>My first paragraph.</p>
    
    </body>
    </html>
```"})