# frozen_string_literal: true

class CreateBlogposts < ActiveRecord::Migration[6.1]
  def change
    create_table :blogposts do |t|
      t.string :author
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
